<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [App\Http\Controllers\gstController::class, 'view'])->name('view');
Route::post('calculate', [App\Http\Controllers\gstController::class, 'calculate'])->name('calculate');
Route::get('clear', [App\Http\Controllers\gstController::class, 'clear'])->name('clear');
