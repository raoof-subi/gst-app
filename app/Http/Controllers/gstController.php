<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class gstController extends Controller
{
    // Load view page
    public function view()
    {
        $data = Session::get('history');
        return view('welcome', ['data' => $data]);
    }
    // Calculations
    public function calculate(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'price' => 'required',
            'gstPer' => 'required'
        ]);
        if ($validatedData->fails()) {
            return (new Response(array(
                'error' => $validatedData->errors()
            )));
        }

        // Calculation
        $originalPrice = $request->price / (1 + ($request->gstPer / 100));
        $SGSTAmount = $CGSTAmount = ($originalPrice * ($request->gstPer / 2)) / 100;
        $response = array(
            'originalPrice' => round($originalPrice, 2),
            'SGSTAmount' => round($SGSTAmount, 2),
            'CGSTAmount' => round($CGSTAmount, 2),
            'totalGST' => round($CGSTAmount + $SGSTAmount, 2)
        );
        Session::push('history', $response);
        return (new Response($response));
    }
    // Clear history
    public function clear()
    {
        Session::forget('history');
        return redirect('/');
    }
}
