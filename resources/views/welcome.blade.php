<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>GST-APPLICATION</title>
        <!-- CSS Libraries -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    </head>
    <style>
        .error{color:red;}
    </style>
    <body>
        <div class="row d-flex">
            {{-- Input form --}}
            <div class="col-md-3 ml-3 mt-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">GST Calculator</h5>
                        <form id="gstCalculator">
                            <div class="form-group">
                              <label for="price">Input price (inclusive of GST)</label>
                              <input type="number" class="form-control" id="price" name="price" placeholder="Enter price">
                            </div>
                            <div class="form-group">
                              <label for="gstPer">GST% (applicable for both SGST and CGST)</label>
                              <input type="number" class="form-control" id="gstPer" name="gstPer" placeholder="Enter GST %">
                            </div>
                            <button type="submit" class="btn btn-primary">Calculate</button>
                        </form>
                    </div>
                </div>
            </div>
            {{-- Result --}}
            <div class="col-md-3 ml-3 mt-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Result</h5>
                        <div id="resultDiv" style="width: 100%;"></div>
                    </div>
                </div>
            </div>
            {{-- History  --}}
            @if(!empty($data))
                <div class="col-md-3 ml-3 mt-3">
                    <div class="card">
                        <div class="card-header" style="width: 100%;">
                            <a class="card-title" style="cursor:pointer;" onclick="openBody()" >History</a>
                            <a style="float: right;" href="{{ route('clear') }}">Clear</a>
                        </div>
                        <div class="card-body" id="bodyHistory" style="display:none;">
                            @foreach ($data as $key => $value)
                                <div>
                                    @foreach ($value as $k => $value1)
                                        <span style="float: left;">{{$k}} :</span><span style="float: right;"> <b>{{ $value1 }}</b></span><br/>
                                    @endforeach
                                    <hr>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> --}}
    <script>
        $(document).ready(function() {
            var csrfToken = $('meta[name="csrf-token"]').attr('content');
            $('#resultDiv').html('<small>No result fount</small>')
            $('#gstCalculator').validate({
                rules: {
                    price: {
                        required: true
                    },
                    gstPer: {
                        required: true,
                        max: 100
                    }
                },
                messages: {
                    price: {
                        required: "Please enter price"
                    },
                    gstPer: {
                        required: "Please enter GST (%)",
                        max: "Please enter a value less than or equal to 100"
                    }
                },
                submitHandler: function(form) {
                    event.preventDefault()
                    var formData = $('#gstCalculator').serialize();
                    $.ajax({
                        url: '{{ route('calculate') }}',
                        type: 'POST',
                        data: {
                            _token: csrfToken,
                            price:$('#price').val(),
                            gstPer:$('#gstPer').val()
                        },
                        success: function(response) {
                            // console.log(response.error.gstPer1);
                            // if(response.error == 'undefined'){
                                var html = '<span style="float: left;">Original price :</span><span style="float: right;"> <b>'+response.originalPrice+'</b></span><br/>'+
                                '<span style="float: left;">SGST :</span><span style="float: right;"> <b>'+response.SGSTAmount+'</b></span><br/>'+
                                '<span style="float: left;">CGST :</span><span style="float: right;"> <b>'+response.CGSTAmount+'</b></span><br/>'+
                                '<span style="float: left;">Total GST :</span><span style="float: right;"> <b>'+response.totalGST+'</b></span><br/>';
                            // }else{
                            //     var html = 'Error!'
                            // }
                            $('#resultDiv').html(html);
                        }
                    });
                }
            });
        });
        function openBody(){
            if(!$('#bodyHistory').is(':visible')){
                $('#bodyHistory').css('display', 'block')
            } else {
                $('#bodyHistory').css('display', 'none')
            }
        }
    </script>
</html>
